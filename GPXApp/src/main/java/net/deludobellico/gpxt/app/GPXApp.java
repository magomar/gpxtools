package net.deludobellico.gpxt.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.deludobellico.gpxt.app.controller.MainViewController;

import java.util.prefs.Preferences;

/**
 * Created by Mario on 07/04/2015.
 */
public class GPXApp extends Application {
    private static final double MIN_WINDOW_WIDTH = 800;
    private static final double MIN_WINDOW_HEIGHT = 600;
    private static final double PREFERRED_WINDOW_WIDTH = 1400;
    private static final double PREFERRED_WINDOW_HEIGHT = 1000;
    private static final Preferences PREFERENCES = Preferences.userRoot().node(GPXApp.class.getName());

    static Stage PRIMARY_STAGE;

    public static void main(String[] args) {
        launch(args);
    }

    static { // use system proxy settings when standalone application
        System.setProperty("java.net.useSystemProxies", "true");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/main-view.fxml"));
        Parent root = loader.load();
        MainViewController mainViewController = loader.getController();
        mainViewController.setStage(primaryStage);
        this.PRIMARY_STAGE = primaryStage;
//        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setTitle("GPX Tools");
        primaryStage.setScene(new Scene(root));
        primaryStage.setMinWidth(MIN_WINDOW_WIDTH);
        primaryStage.setMinHeight(MIN_WINDOW_HEIGHT);
        primaryStage.setWidth(PREFERENCES.getDouble("window-width", PREFERRED_WINDOW_WIDTH));
        primaryStage.setHeight(PREFERENCES.getDouble("window-height", PREFERRED_WINDOW_HEIGHT));
        Image appIcon = new Image(getClass().getClassLoader().getResourceAsStream("images/app-icon.png"));
        primaryStage.getIcons().add(appIcon);
        primaryStage.show();

    }

    @Override
    public void stop() {
        PREFERENCES.putDouble("window-width", PRIMARY_STAGE.getWidth());
        PREFERENCES.putDouble("window-height", PRIMARY_STAGE.getHeight());
    }

}
