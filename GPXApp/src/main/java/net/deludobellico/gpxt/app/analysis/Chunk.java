package net.deludobellico.gpxt.app.analysis;

import net.deludobellico.gpxt.app.model.gpx.Track;
import net.deludobellico.gpxt.app.model.gpx.TrackSegment;
import net.deludobellico.gpxt.app.model.gpx.Waypoint;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mario on 26/05/2015.
 * A chunk contains information about two consecutive waypoints
 */
public class Chunk {
    private double distance; //meters
    private double speed; //meters / second
    private Duration duration;
    private Duration movingTime;
    private double grade; // percentual
    private double climb; // absolute positive increment of height
    private double drop; // absolute negative increment of height
    private Waypoint firstPoint;
    private Waypoint lastPoint;
    private List<Chunk> chunks = new ArrayList<>();
    private double avgHeight;
    private double avgCadence;
    private double avgHeartRate;

    public Chunk(Waypoint w1, Waypoint w2, DistanceFunction distanceFunction, double speedThreshold) {
        firstPoint = w1;
        lastPoint = w2;
        distance = distanceFunction.getDistance(w1.getLatitude(), w1.getLongitude(),
                w2.getLatitude(), w2.getLongitude());
        duration = Duration.between(w1.getTime(), w2.getTime());
        speed = distance / duration.getSeconds();
        if (speed > speedThreshold) {
            movingTime = Duration.between(w1.getTime(), w2.getTime());
        } else {
            movingTime = Duration.ofSeconds(0);
        }
        climb = w2.getElevation() - w1.getElevation();
        grade = climb / distance * 100;
        avgHeight = (w1.getElevation() + w2.getElevation()) / 2;
        if (null != w1.getTrackPointExtension() && null != w2.getTrackPointExtension()) {
            avgCadence = (w1.getTrackPointExtension().getCad() + w2.getTrackPointExtension().getCad()) / 2;
            avgHeartRate = (w1.getTrackPointExtension().getHr() + w2.getTrackPointExtension().getHr()) / 2;
        }
    }

    public Chunk(Waypoint waypoint) {
        distance = 0;
        duration = Duration.ofSeconds(0);
        speed = 0;
        climb = 0;
        grade = 0;
        movingTime = Duration.ofSeconds(0);
        firstPoint = waypoint;
        lastPoint = waypoint;
    }

    public Chunk(Track track, DistanceFunction distanceFunction, double speedThreshold) {
        distance = 0;
        duration = Duration.ofSeconds(0);
        movingTime = Duration.ofSeconds(0);
        climb = 0;
        drop = 0;
        firstPoint = track.getStartPoint();
        lastPoint = track.getEndPoint();
        Waypoint previous = firstPoint;
        chunks.add(new Chunk(firstPoint));
        for (TrackSegment trackSegment : track.getTrackSegments())
            for (Waypoint waypoint : trackSegment.getWaypoints()) {
                if (!waypoint.equals(firstPoint)) {
                    Chunk chunk = new Chunk(previous, waypoint, distanceFunction, speedThreshold);
                    previous = waypoint;
                    chunks.add(chunk);
                    duration = duration.plus(chunk.getDuration());
                    movingTime = movingTime.plus(chunk.getMovingTime());
                    distance += chunk.getDistance();
                    if (chunk.getClimb() >= 0) {
                        climb += chunk.getClimb();
                    } else {
                        drop += Math.abs(chunk.getClimb());
                    }
                }
            }
        speed = distance / getDuration().getSeconds();
    }

    public double getDistance() {
        return distance;
    }

    public double getSpeed() {
        return speed;
    }

    public Duration getDuration() {
        return duration;
    }

    public double getGrade() {
        return grade;
    }

    public double getClimb() {
        return climb;
    }

    public double getDrop() {
        return drop;
    }

    public Duration getMovingTime() {
        return movingTime;
    }

    public double getAvgCadence() {
        return avgCadence;
    }

    public double getAvgHeartRate() {
        return avgHeartRate;
    }

    public double getAvgHeight() {
        return avgHeight;
    }

    public Waypoint getFirstPoint() {
        return firstPoint;
    }

    public Waypoint getLastPoint() {
        return lastPoint;
    }

    public List<Chunk> getChunks() {
        return chunks;
    }
}
