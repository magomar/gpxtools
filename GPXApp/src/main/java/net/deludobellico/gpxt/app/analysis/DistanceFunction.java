package net.deludobellico.gpxt.app.analysis;

import net.deludobellico.gpxt.app.model.gpx.Waypoint;

/**
 * Created by Mario on 26/05/2015.
 */
public interface DistanceFunction {
    /*
    Gets the distance between two points defined by latitude and longitude
     */
    double getDistance(double latitude1, double longitude1, double latitude2, double longitude2);
}
