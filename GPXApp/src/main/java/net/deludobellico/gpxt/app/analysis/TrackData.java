package net.deludobellico.gpxt.app.analysis;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import net.deludobellico.gpxt.app.util.DateTimeUtils;
import net.deludobellico.gpxt.app.model.gpx.Track;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by Mario on 25/05/2015.
 */
public class TrackData {

    private final ObjectProperty<LocalDateTime> startTime = new SimpleObjectProperty<>();
    private final ObjectProperty<LocalDateTime> endTime = new SimpleObjectProperty<>();
    private final ObjectProperty<Duration> totalDuration = new SimpleObjectProperty<>();
    private final DoubleProperty totalDistance = new SimpleDoubleProperty();
    private final DoubleProperty totalClimb = new SimpleDoubleProperty();
    private final DoubleProperty totalDrop = new SimpleDoubleProperty();
    private final IntegerProperty averageHeight = new SimpleIntegerProperty();
    private final IntegerProperty minHeight = new SimpleIntegerProperty();
    private final IntegerProperty maxHeight = new SimpleIntegerProperty();
    private final DoubleProperty averageSpeed = new SimpleDoubleProperty();
    private final DoubleProperty maxSpeed = new SimpleDoubleProperty();
    private final ObjectProperty<Duration> movingTime = new SimpleObjectProperty<>();
    private final IntegerProperty averageCadence = new SimpleIntegerProperty();
    private final IntegerProperty maxCadence = new SimpleIntegerProperty();
    private final IntegerProperty averageHeartrate = new SimpleIntegerProperty();
    private final IntegerProperty minHeartRate = new SimpleIntegerProperty();
    private final IntegerProperty maxHeartrate = new SimpleIntegerProperty();
    private final DoubleProperty speedThreshold = new SimpleDoubleProperty();
    private final ObservableList<Chunk> chunks = FXCollections.observableArrayList();
//    private final DoubleProperty positiveGrade = new SimpleDoubleProperty();
//    private final DoubleProperty negativeGrade = new SimpleDoubleProperty();

    private final IntegerProperty numPoints = new SimpleIntegerProperty();

    private ObservableList<XYChart.Series<Number, Integer>> elevationPerDuration = FXCollections.observableArrayList();
    private ObservableList<XYChart.Series<Number, Integer>> elevationPerDistance = FXCollections.observableArrayList();
    private ObservableList<XYChart.Series<Number, Double>> speedPerDuration = FXCollections.observableArrayList();
    private ObservableList<XYChart.Series<Number, Double>> speedPerDistance = FXCollections.observableArrayList();

    public TrackData(Track track, double speedThreshold, double heartRateThreshold) {
        XYChart.Series<Number, Integer> elevationPerDurationSeries = new XYChart.Series<>();
        XYChart.Series<Number, Integer> elevationPerDistanceSeries = new XYChart.Series<>();
        XYChart.Series<Number, Double> speedPerDurationSeries = new XYChart.Series<>();
        XYChart.Series<Number, Double> speedPerDistanceSeries = new XYChart.Series<>();

        double accDuration = 0; //in minutes (with decimals)
        double accDistance = 0; // in Km
        double maxSpeed = Double.MIN_VALUE; // in m/s
        double avgHeight = 0; // in meters
        double minHeight = Double.MAX_VALUE;
        double maxHeight = Double.MIN_VALUE;
        double avgCadence = 0; // pedalying frecuency (per minute)
        double maxCad = Double.MIN_VALUE;
        double maxHeartRate = Double.MIN_VALUE; // heart beats per minute
        double minHr = Double.MAX_VALUE;
        double avgHearRate = 0;

        Chunk trackChunk = new Chunk(track, GPSDistanceFunction.HAVERSHINE, speedThreshold);
        chunks.addAll(trackChunk.getChunks());
        for (Chunk chunk :chunks ) {
            accDuration += Double.valueOf(chunk.getDuration().getSeconds()) / 60.0; //in minutes
            accDistance += Double.valueOf(chunk.getDistance())/1000; //in Km
            double elevation = chunk.getLastPoint().getElevation(); //in meters
            double speed = chunk.getSpeed(); //in Km/h meters per second
            if (speed > maxSpeed) maxSpeed = speed;
            speed = speed * 3.6;
            if (elevation > maxHeight) maxHeight = elevation;
            else if (elevation < minHeight) minHeight = elevation;
            avgHeight += chunk.getAvgHeight() * chunk.getDistance();
            avgCadence += chunk.getAvgCadence() * chunk.getDistance();
            avgHearRate += chunk.getAvgHeartRate() * chunk.getDistance();
            if (chunk.getAvgCadence() > maxCad) maxCad = chunk.getAvgCadence();
            if (chunk.getAvgHeartRate() > maxHeartRate) maxHeartRate = chunk.getAvgHeartRate();
            else if (chunk.getAvgHeartRate() < minHr && chunk.getAvgHeartRate() > heartRateThreshold) minHr = chunk.getAvgHeartRate();
            elevationPerDurationSeries.getData().add(new XYChart.Data<>(accDuration,Double.valueOf(elevation).intValue()));
            elevationPerDistanceSeries.getData().add(new XYChart.Data<>(accDistance,Double.valueOf(elevation).intValue()));
            speedPerDurationSeries.getData().add(new XYChart.Data<>(accDuration, speed));
            speedPerDistanceSeries.getData().add(new XYChart.Data<>(accDistance, speed));
//            System.out.println(String.format("Duration= %.2f, Distance= %.2f, Height= %d, Speed= %f", accDuration, accDistance, elevation, speed));
        }

        startTime.set(trackChunk.getFirstPoint().getTime());
        endTime.set(trackChunk.getLastPoint().getTime());
        totalDuration.set(trackChunk.getDuration());
        movingTime.set(trackChunk.getMovingTime());
        totalDistance.set(trackChunk.getDistance());
        totalClimb.set(trackChunk.getClimb());
        totalDrop.set(trackChunk.getDrop());
        averageHeight.set((int) (avgHeight / trackChunk.getDistance()));
        this.minHeight.set((int) minHeight);
        this.maxHeight.set((int) maxHeight);
        averageCadence.set((int) (avgCadence /trackChunk.getDistance()));
        maxCadence.set((int) maxCad);
        averageHeartrate.set((int) (avgHearRate/trackChunk.getDistance()));
        if (minHr < 250.0) minHeartRate.set((int) minHr);
        this.maxHeartrate.set((int) maxHeartRate);
        averageSpeed.set(trackChunk.getDistance() / trackChunk.getMovingTime().getSeconds());
        this.maxSpeed.set(maxSpeed);
        numPoints.set(trackChunk.getChunks().size());

        elevationPerDuration.clear();
        elevationPerDuration.addAll(elevationPerDurationSeries);
        elevationPerDistance.clear();
        elevationPerDistance.addAll(elevationPerDistanceSeries);
        speedPerDuration.clear();
        speedPerDuration.addAll(speedPerDurationSeries);
        speedPerDistance.clear();
        speedPerDistance.addAll(speedPerDistanceSeries);

        System.out.println(this.toString());

    }

    public LocalDateTime getStartTime() {
        return startTime.get();
    }

    public ObjectProperty<LocalDateTime> startTimeProperty() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime.get();
    }

    public ObjectProperty<LocalDateTime> endTimeProperty() {
        return endTime;
    }

    public Duration getTotalDuration() {
        return totalDuration.get();
    }

    public ObjectProperty<Duration> totalDurationProperty() {
        return totalDuration;
    }

    public double getTotalDistance() {
        return totalDistance.get();
    }

    public DoubleProperty totalDistanceProperty() {
        return totalDistance;
    }

    public double getTotalClimb() {
        return totalClimb.get();
    }

    public DoubleProperty totalClimbProperty() {
        return totalClimb;
    }

    public double getTotalDrop() {
        return totalDrop.get();
    }

    public DoubleProperty totalDropProperty() {
        return totalDrop;
    }

    public int getAverageHeight() {
        return averageHeight.get();
    }

    public IntegerProperty averageHeightProperty() {
        return averageHeight;
    }

    public int getMinHeight() {
        return minHeight.get();
    }

    public IntegerProperty minHeightProperty() {
        return minHeight;
    }

    public int getMaxHeight() {
        return maxHeight.get();
    }

    public IntegerProperty maxHeightProperty() {
        return maxHeight;
    }

    public double getAverageSpeed() {
        return averageSpeed.get();
    }

    public DoubleProperty averageSpeedProperty() {
        return averageSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed.get();
    }

    public DoubleProperty maxSpeedProperty() {
        return maxSpeed;
    }

    public Duration getMovingTime() {
        return movingTime.get();
    }

    public ObjectProperty<Duration> movingTimeProperty() {
        return movingTime;
    }

    public int getAvgCadence() {
        return averageCadence.get();
    }

    public IntegerProperty avgCadenceProperty() {
        return averageCadence;
    }

    public int getMaxCadence() {
        return maxCadence.get();
    }

    public IntegerProperty maxCadenceProperty() {
        return maxCadence;
    }

    public int getAvgHeartrate() {
        return averageHeartrate.get();
    }

    public IntegerProperty avgHeartrateProperty() {
        return averageHeartrate;
    }

    public int getMinHeartRate() {
        return minHeartRate.get();
    }

    public IntegerProperty minHeartRateProperty() {
        return minHeartRate;
    }

    public ObservableList<Chunk> getChunks() {
        return chunks;
    }

    public int getMaxHeartrate() {
        return maxHeartrate.get();
    }

    public IntegerProperty maxHeartrateProperty() {
        return maxHeartrate;
    }

    public double getSpeedThreshold() {
        return speedThreshold.get();
    }

    public DoubleProperty speedThresholdProperty() {
        return speedThreshold;
    }

    public int getNumPoints() {
        return numPoints.get();
    }

    public IntegerProperty numPointsProperty() {
        return numPoints;
    }

    public ObservableList<XYChart.Series<Number, Integer>> getElevationPerDuration() {
        return elevationPerDuration;
    }

    public ObservableList<XYChart.Series<Number, Integer>> getElevationPerDistance() {
        return elevationPerDistance;
    }


    public ObservableList<XYChart.Series<Number, Double>> getSpeedPerDuration() {
        return speedPerDuration;
    }

    public ObservableList<XYChart.Series<Number, Double>> getSpeedPerDistance() {
        return speedPerDistance;
    }

    @Override
    public String toString() {
        return "TrackData{" +
                "startTime=" + DateTimeUtils.format(startTime.get()) +
                ", endTime=" + DateTimeUtils.format(endTime.get()) +
                String.format(", totalDistance=%.2f", totalDistance.get()) +
                ", totalDuration=" + DateTimeUtils.format(totalDuration.get()) +
                String.format(", averageSpeed=%.2f", averageSpeed.get()) +
                String.format(", totalClimb=%.2f", totalClimb.get()) +
                String.format(", totalDrop=%.2f", totalDrop.get()) +
                '}';
    }
}
