package net.deludobellico.gpxt.app.controller;

/**
 * Created by Mario on 08/04/2015.
 */

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import net.deludobellico.gpxt.app.model.gpx.Gpx;
import net.deludobellico.gpxt.data.jaxb.GpxType;
import net.deludobellico.gpxt.util.FileIO;
import net.deludobellico.gpxt.util.GPXFileType;
import net.deludobellico.gpxt.util.JAXBFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class DataLoaderController extends AbstractModuleController<VBox> {
    private static JAXBFactory JAXB;

    static {
        try {
            JAXB = new JAXBFactory(GpxType.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private TextField folderPathTextField;

    @FXML
    private Button clearButton;

    @FXML
    private Button loadButton;

    @FXML
    private Button saveButton;

    @FXML
    private VBox contentPane;

    @FXML
    private ListView<File> filesListView;

    @FXML
    private Button selectFolderButton;

    @FXML
    private ListView<File> selectedFilesListView;

    private ObservableList<File> files = FXCollections.observableArrayList();
    private ObservableList<File> selectedFiles = FXCollections.observableArrayList();
    private BooleanProperty dataLoaded = new SimpleBooleanProperty(false);
    private BooleanProperty filesSelected = new SimpleBooleanProperty(false);
    private ObservableList<Gpx> gpxList = FXCollections.observableArrayList();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert folderPathTextField != null : "fx:id=\"folderPathTextField\" was not injected: check your FXML file 'data-loader.fxml'.";
        assert clearButton != null : "fx:id=\"clearButton\" was not injected: check your FXML file 'data-loader.fxml'.";
        assert loadButton != null : "fx:id=\"loadButton\" was not injected: check your FXML file 'data-loader.fxml'.";
        assert saveButton != null : "fx:id=\"saveButton\" was not injected: check your FXML file 'data-loader.fxml'.";
        assert contentPane != null : "fx:id=\"contentPane\" was not injected: check your FXML file 'data-loader.fxml'.";
        assert filesListView != null : "fx:id=\"filesListView\" was not injected: check your FXML file 'data-loader.fxml'.";
        assert selectFolderButton != null : "fx:id=\"selectFolderButton\" was not injected: check your FXML file 'data-loader.fxml'.";
        assert selectedFilesListView != null : "fx:id=\"selectedFilesListView\" was not injected: check your FXML file 'data-loader.fxml'.";

        filesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        filesListView.setCellFactory(param -> new FileCellFactory());
        selectedFilesListView.setCellFactory(param -> new FileCellFactory());
        filesListView.setItems(files);
        selectedFilesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        selectedFilesListView.setItems(selectedFiles);
        File defaultDirectory = FileIO.getFile("data");
        folderPathTextField.setText(defaultDirectory.getPath());
        refreshFolder();
        selectedFiles.addListener((ListChangeListener<File>) c -> {
            filesSelected.set(!selectedFiles.isEmpty());
        });
        loadButton.disableProperty().bind(dataLoaded.or(filesSelected.not()));
        saveButton.disableProperty().bind(dataLoaded.not());
        clearButton.disableProperty().bind(dataLoaded.not());
    }

    private void refreshFolder() {
        String folderPath = folderPathTextField.getText();
        if (!folderPathTextField.getText().equals("")) {
            files.clear();
            File folder = new File(folderPath);
            List<File> fileList = FileIO.listFiles(folder, GPXFileType.GPX.getFileTypeFilter(), false);
            files.addAll(fileList);
        }
    }

    @FXML
    private void selectFolderAction(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Chose a directory containing data");
        File defaultDirectory = FileIO.getFile("data");
        directoryChooser.setInitialDirectory(defaultDirectory);
        //Show open file dialog
        File file = directoryChooser.showDialog(null);
        if (file != null) {
            folderPathTextField.setText(file.getPath());
            refreshFolder();
        }
    }

    @FXML
    private void selectSome(ActionEvent event) {
        List<File> selectedFileList = filesListView.getSelectionModel().getSelectedItems();
        selectedFiles.addAll(selectedFileList);
        files.removeAll(selectedFileList);
    }

    @FXML
    private void unselectSome(ActionEvent event) {
        List<File> selectedFileList = selectedFilesListView.getSelectionModel().getSelectedItems();
        files.addAll(selectedFileList);
        selectedFiles.removeAll(selectedFileList);
    }

    @FXML
    private void selectAll(ActionEvent event) {
        selectedFiles.addAll(files);
        files.clear();
    }

    @FXML
    private void unselectAll(ActionEvent event) {
        files.addAll(selectedFiles);
        selectedFiles.clear();
    }

    @FXML
    private void loadData(ActionEvent event) {
        if (selectedFiles.isEmpty()) return;
        for (File selectedFile : selectedFiles) {
            String extension = FileIO.getExtension(selectedFile);
            GpxType gpx = null;
            switch (extension) {
                case "gpx":
                    gpx = (GpxType) JAXB.unmarshallXML(selectedFile);
                    break;
                case "zip":
                    gpx = (GpxType) JAXB.unmarshallZipped(selectedFile);
                    break;
            }
            if (gpx == null) continue;
            gpxList.add(new Gpx(gpx));
            System.out.println("GPX file loaded: " + selectedFile.getName());
        }
        dataLoaded.set(true);
    }

    @FXML
    void saveData(ActionEvent event) {
        if (selectedFiles.isEmpty()) return;
        // TODO

    }

    @FXML
    void clearData(ActionEvent event) {
        selectedFiles.clear();
        refreshFolder();
        // TODO
        dataLoaded.set(false);
    }

    public ObservableList<Gpx> getGpxList() {
        return gpxList;
    }

    public BooleanProperty dataLoadedProperty() {
        return dataLoaded;
    }

    private static class FileCellFactory extends ListCell<File> {
        HBox hbox = new HBox();
        Label label = new Label();

        public FileCellFactory() {
            super();
            label.setPrefWidth(200);
            hbox.getChildren().addAll(label);
//            HBox.setHgrow(pane, Priority.ALWAYS);
        }

        @Override
        protected void updateItem(File item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);  // No text in label of super class
            if (empty) {
                setGraphic(null);
            } else {
                String fileName = item.getName();
                label.setText(item.getName());
                setGraphic(hbox);
            }
        }
    }
}

