package net.deludobellico.gpxt.app.controller;

import javafx.beans.property.BooleanProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import net.deludobellico.gpxt.app.model.gpx.Gpx;
import net.deludobellico.gpxt.app.model.gpx.Track;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * Created by Mario on 22/02/2015.
 */
public class MainViewController implements Initializable {

    private static final Logger LOG = Logger.getLogger(MainViewController.class.getName());

    @FXML
    private StackPane stackPane;

    @FXML
    private Button connectionsButton;

    @FXML
    private Button viewerButton;

    @FXML
    private ToolBar mainToolBar;

    private final Map<View, Parent> moduleViews = new HashMap<>(View.values().length);
    private final Map<View, ModuleController> moduleControllers = new HashMap<>(View.values().length);
    private View currentView;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Preload all modules
        for (View view : View.values()) {
            loadModule(view);
        }
        loadViewerView();
   }

    public void setStage(Stage stage) {
        final Delta dragDelta = new Delta();
        mainToolBar.setOnMousePressed(mouseEvent -> {
            // record a delta distance for the drag and drop operation.
            dragDelta.x = stage.getX() - mouseEvent.getScreenX();
            dragDelta.y = stage.getY() - mouseEvent.getScreenY();
        });
        mainToolBar.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                stage.setX(mouseEvent.getScreenX() + dragDelta.x);
                stage.setY(mouseEvent.getScreenY() + dragDelta.y);
            }
        });
    }

    @FXML
    private void loadDataLoaderView() {
        loadView(View.DATA_LOADER);
    }

    @FXML
    private void loadConnectionsView() {
        loadView(View.CONNECTIONS);
    }

    @FXML
    private void loadViewerView() {
        loadView(View.VIEWER);
    }

    private void loadModule(View view) {
        if (moduleViews.containsKey(view)) return;
        else {
            FXMLLoader fxmlLoader = new FXMLLoader(MainViewController.class.getResource(view.fxmlResourcePath));
            Parent moduleContentPane = null;
            try {
                moduleContentPane = fxmlLoader.load();
                moduleContentPane.setOpacity(0);
                moduleViews.put(view, moduleContentPane);
                moduleControllers.put(view, fxmlLoader.getController());
                stackPane.getChildren().add(moduleContentPane);
            } catch (IOException e) {
                LOG.severe("Error loading module " + view.name());
                LOG.severe(e.getMessage());
            }

        }
    }

    private void loadView(View view) {
        if (currentView != null) moduleViews.get(currentView).setOpacity(0);
        currentView = view;
        Parent node = moduleViews.get(view);
        stackPane.getChildren().remove(node);
        stackPane.getChildren().add(node);
        node.setOpacity(1);
    }

    private enum View {
        DATA_LOADER("data-loader.fxml"),
        CONNECTIONS("map.fxml"),
        VIEWER("viewer.fxml");
        private final String fxmlResourcePath;

        View(String fxmlFilename) {
            fxmlResourcePath = "/view/" + fxmlFilename;
        }
    }

    private class Delta {
        double x, y;
    }
}
