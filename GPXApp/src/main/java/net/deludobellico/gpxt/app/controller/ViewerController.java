package net.deludobellico.gpxt.app.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import net.deludobellico.gpxt.app.util.DateTimeUtils;
import net.deludobellico.gpxt.app.analysis.TrackData;
import net.deludobellico.gpxt.app.model.gpx.Track;
import net.deludobellico.gpxt.data.jaxb.GpxType;
import net.deludobellico.gpxt.data.jaxb.TrackPointExtensionT;
import net.deludobellico.gpxt.util.FileIO;
import net.deludobellico.gpxt.util.GPXFileType;
import net.deludobellico.gpxt.util.JAXBFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Mario on 23/05/2015.
 */
public class ViewerController extends AbstractModuleController<BorderPane> {

    private final static double DEFAULT_SPEED_THRESHOLD = 1.0;
    private final static double DEFAULT_HEART_RATE_THRESHOLD = 40.0;

    private static JAXBFactory JAXB;

    static {
        try {
            JAXB = new JAXBFactory(GpxType.class, TrackPointExtensionT.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private TextField name;

    @FXML
    private TextField date;

    @FXML
    private TextField startTime;

    @FXML
    private TextField endTime;

    @FXML
    private TextField duration;

    @FXML
    private TextField distance;

    @FXML
    private TextField avgHeight;

    @FXML
    private TextField minHeight;

    @FXML
    private TextField maxHeight;

    @FXML
    private TextField elevation;

    @FXML
    private Slider speedThreshold;

    @FXML
    private TextField avgSpeed;

    @FXML
    private TextField maxSpeed;

    @FXML
    private TextField movingTime;

    @FXML
    private TextField avgCadence;

    @FXML
    private TextField maxCadence;

    @FXML
    private TextField avgHeartrate;

    @FXML
    private TextField minHeartrate;

    @FXML
    private TextField maxHeartrate;

    @FXML
    private TextField numPoints;

    @FXML
    private LineChart<Number, Integer> elevationPerDistanceChart;

    @FXML
    private LineChart<Number, Integer> elevationPerDurationChart;

    @FXML
    private LineChart<Number, Double> speedPerDistanceChart;

    @FXML
    private LineChart<Number, Double> speedPerDurationChart;

    @FXML
    private MapController trackMapController;

    @FXML
    private ListView<File> tracks;

    private ObservableList<File> trackFiles = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tracks.setCellFactory(param -> new FileCellFactory());
        showGPXFiles();
        tracks.setItems(trackFiles);
        tracks.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("Loading " + newValue.getName());
            loadGPXFile(newValue);
        });
        speedThreshold.setValue(DEFAULT_SPEED_THRESHOLD);
    }

    public void setTrack(Track track) {
        TrackData data = new TrackData(track, speedThreshold.getValue(), DEFAULT_HEART_RATE_THRESHOLD);
        name.setText(track.getName());
        date.setText(DateTimeUtils.format(data.getStartTime().toLocalDate()));
        startTime.setText(DateTimeUtils.format(data.getStartTime().toLocalTime()));
        endTime.setText(DateTimeUtils.format(data.getEndTime()));
        duration.setText(DateTimeUtils.format(data.getTotalDuration()));
        movingTime.setText(DateTimeUtils.format(data.getMovingTime()));
        distance.setText(String.format("%.3f Km", data.getTotalDistance() / 1000));
        avgHeight.setText(String.valueOf(data.getAverageHeight()) + " m");
        minHeight.setText(String.valueOf(data.getMinHeight()) + " m");
        maxHeight.setText(String.valueOf(data.getMaxHeight()) + " m");
        avgSpeed.setText(String.format("%.2f Km/h", data.getAverageSpeed() * 3.6));
        maxSpeed.setText(String.format("%.2f Km/h", data.getMaxSpeed() * 3.6));
        elevation.setText(String.format("+%.0f / -%.0f m", data.getTotalClimb(), data.getTotalDrop()));
        numPoints.setText(String.valueOf(data.getNumPoints()));
        avgCadence.setText(String.valueOf(data.getAvgCadence()));
        maxCadence.setText(String.valueOf(data.getMaxCadence()));
        avgHeartrate.setText(String.valueOf(data.getAvgHeartrate()));
        minHeartrate.setText(String.valueOf(data.getMinHeartRate()));
        maxHeartrate.setText(String.valueOf(data.getMaxHeartrate()));
        elevationPerDistanceChart.getData().clear();
        elevationPerDistanceChart.getData().addAll(data.getElevationPerDistance());
        elevationPerDurationChart.getData().clear();
        elevationPerDurationChart.getData().addAll(data.getElevationPerDuration());
        speedPerDistanceChart.getData().clear();
        speedPerDistanceChart.getData().addAll(data.getSpeedPerDistance());
        speedPerDurationChart.getData().clear();
        speedPerDurationChart.getData().addAll(data.getSpeedPerDuration());
        trackMapController.showTrackOnMap(data);

    }


    private void showGPXFiles() {
        File defaultDirectory = FileIO.getFile("data");
        String folderPath = defaultDirectory.getPath();
        if (!folderPath.equals("")) {
            trackFiles.clear();
            File folder = new File(folderPath);
            List<File> fileList = FileIO.listFiles(folder, GPXFileType.GPX.getFileTypeFilter(), false);
            trackFiles.addAll(fileList);
        }
    }

    private void loadGPXFile(File file) {
        if (null == file) return;
        String extension = FileIO.getExtension(file);
        GpxType gpx = null;
        switch (extension) {
            case "gpx":
                gpx = (GpxType) JAXB.unmarshallXML(file);
                break;
            case "zip":
                gpx = (GpxType) JAXB.unmarshallZipped(file);
                break;
        }
        if (gpx != null) {
            setTrack(new Track(gpx.getTrk().get(0)));
        } else System.out.println("Error loading GPX data from: " + file.getName());
    }

    private static class FileCellFactory extends ListCell<File> {
        HBox hbox = new HBox();
        Label label = new Label();

        public FileCellFactory() {
            super();
            label.setPrefWidth(200);
            hbox.getChildren().addAll(label);
//            HBox.setHgrow(pane, Priority.ALWAYS);
        }

        @Override
        protected void updateItem(File item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);  // No text in label of super class
            if (empty) {
                setGraphic(null);
            } else {
                String fileName = item.getName();
                label.setText(item.getName());
                setGraphic(hbox);
            }
        }
    }
}
