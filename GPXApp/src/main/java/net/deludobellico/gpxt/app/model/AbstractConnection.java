package net.deludobellico.gpxt.app.model;

/**
 * Created by Mario on 09/04/2015.
 */
public class AbstractConnection implements Connection {
    private final String username;

    public AbstractConnection(String username) {
        this.username = username;
    }
}
