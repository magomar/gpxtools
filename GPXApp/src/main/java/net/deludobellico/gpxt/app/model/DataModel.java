package net.deludobellico.gpxt.app.model;

/**
 * Any class that is able a provide data of type {@link D}
 */
public interface DataModel<D> {
    /**
     *
     * @return an objet of type <code>D</code>
     */
    D getData();
}
