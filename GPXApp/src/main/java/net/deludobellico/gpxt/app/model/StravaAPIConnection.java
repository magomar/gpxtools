package net.deludobellico.gpxt.app.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by Mario on 09/04/2015.
 */
public class StravaAPIConnection extends AbstractConnection {
    private StringProperty key = new SimpleStringProperty();
    private StringProperty token = new SimpleStringProperty();

    public StravaAPIConnection(String username) {
        super(username);
        key.set("231651146aa53123ab97d2cfd964519c6940cca4");
        token.set("3894243108b2a4ae68d8931feb4bffe7a377dd6c");
    }

    public String getKey() {
        return key.get();
    }

    public StringProperty keyProperty() {
        return key;
    }

    public void setKey(String key) {
        this.key.set(key);
    }

    public String getToken() {
        return token.get();
    }

    public StringProperty tokenProperty() {
        return token;
    }

    public void setToken(String token) {
        this.token.set(token);
    }
}
