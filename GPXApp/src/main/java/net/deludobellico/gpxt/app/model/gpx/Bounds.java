package net.deludobellico.gpxt.app.model.gpx;

import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.BoundsType;

import java.math.BigDecimal;

/**
 * @author Mario G�mez
 * Two lat/lon pairs defining the extent of an element.
 */
public class Bounds implements DataModel<BoundsType> {

    protected BigDecimal minlat;
    protected BigDecimal minlon;
    protected BigDecimal maxlat;
    protected BigDecimal maxlon;

    @Override
    public BoundsType getData() {
        return null;
    }
}
