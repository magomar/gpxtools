package net.deludobellico.gpxt.app.model.gpx;

import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.CopyrightType;

import java.time.LocalDate;

/**
 * @author Mario G�mez
 * Information about the copyright holder and any license governing use of this file. By linking to an appropriate
 * license, you may place your data into the public domain or grant additional usage rights.
 */
public class Copyright implements DataModel<CopyrightType> {
    protected LocalDate year;
    protected String license;
    protected String author;

    @Override
    public CopyrightType getData() {
        return null;
    }
}
