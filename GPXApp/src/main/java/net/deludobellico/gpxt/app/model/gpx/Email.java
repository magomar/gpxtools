package net.deludobellico.gpxt.app.model.gpx;

import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.EmailType;

/**
 * @author Mario G�mez
 * An email address. Broken into two parts (id and domain) to help prevent email harvesting.
 */
public class Email implements DataModel<EmailType> {
    protected String id;
    protected String domain;

    @Override
    public EmailType getData() {
        return null;
    }
}
