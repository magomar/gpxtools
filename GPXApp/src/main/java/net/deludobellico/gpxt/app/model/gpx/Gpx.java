package net.deludobellico.gpxt.app.model.gpx;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.GpxType;

import java.util.stream.Collectors;

/**
 * @author Mario G�mez
 * GPX documents contain a metadata header, followed by waypoints, routes, and tracks.
 * It can contain also application specific extensions.
 */
public class Gpx implements DataModel<GpxType> {

    protected Metadata metadata;
    protected ObservableList<Waypoint> waypoints = FXCollections.observableArrayList();
    protected ObservableList<Route> routes = FXCollections.observableArrayList();
    protected ObservableList<Track> tracks = FXCollections.observableArrayList();
    protected Extensions extensions;
    protected StringProperty version = new SimpleStringProperty();
    protected StringProperty creator = new SimpleStringProperty();

    public Gpx(GpxType gpx) {
        metadata = new Metadata(gpx.getMetadata());
        waypoints.addAll(gpx.getWpt().stream().map(Waypoint::new).collect(Collectors.toList()));
        extensions = new Extensions(gpx.getExtensions());
        routes.addAll(gpx.getRte().stream().map(Route::new).collect(Collectors.toList()));
        tracks.addAll(gpx.getTrk().stream().map(Track::new).collect(Collectors.toList()));
    }

    @Override
    public GpxType getData() {
        GpxType gpx = new GpxType();
        gpx.setMetadata(metadata.getData());
        gpx.getWpt().addAll(waypoints.stream().map(Waypoint::getData).collect(Collectors.toList()));
        gpx.getRte().addAll(routes.stream().map(Route::getData).collect(Collectors.toList()));
        gpx.getTrk().addAll(tracks.stream().map(Track::getData).collect(Collectors.toList()));
        gpx.setExtensions(extensions.getData());
        gpx.setVersion(version.get());
        gpx.setCreator(creator.get());
        return null;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public ObservableList<Waypoint> getWaypoints() {
        return waypoints;
    }

    public ObservableList<Route> getRoutes() {
        return routes;
    }

    public ObservableList<Track> getTracks() {
        return tracks;
    }

    public Extensions getExtensions() {
        return extensions;
    }

    public String getVersion() {
        return version.get();
    }

    public StringProperty versionProperty() {
        return version;
    }

    public String getCreator() {
        return creator.get();
    }

    public StringProperty creatorProperty() {
        return creator;
    }


}
