package net.deludobellico.gpxt.app.model.gpx;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.LinkType;

/**
 * @author Mario G�mez
 * A links to an external resource (Web page, digital photo, video clip, etc) with additional information.
 */
public class Link implements DataModel<LinkType> {
    protected StringProperty text = new SimpleStringProperty();
    protected StringProperty type = new SimpleStringProperty();
    protected StringProperty href = new SimpleStringProperty();

    public Link(LinkType link) {
        text.set(link.getText());
        type.set(link.getType());
        href.set(link.getHref());
    }

    @Override
    public LinkType getData() {
        LinkType link = new LinkType();
        link.setText(text.get());
        link.setType(type.get());
        link.setHref(href.get());
        return link;
    }
}
