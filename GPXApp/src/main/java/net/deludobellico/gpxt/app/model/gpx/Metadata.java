package net.deludobellico.gpxt.app.model.gpx;

import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.MetadataType;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mario G�mez
 * Information about the GPX file, author, and copyright restrictions goes in the metadata section. Providing rich,
 * meaningful information about your GPX files allows others to search for and use your GPS data.
 */
public class Metadata implements DataModel<MetadataType> {
    protected String name;
    protected String desc;
    protected Person author;
    protected Copyright copyright;
    protected List<Link> link;
    protected LocalDateTime time;
    protected String keywords;
    protected Bounds bounds;
    protected Extensions extensions;

    public Metadata(MetadataType metadata) {
        // TODO
    }

    @Override
    public MetadataType getData() {
        MetadataType metadata = new MetadataType();
        // TODO
        return metadata;
    }
}
