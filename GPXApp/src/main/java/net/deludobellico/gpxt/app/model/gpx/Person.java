package net.deludobellico.gpxt.app.model.gpx;

import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.PersonType;

/**
 * @author Mario G�mez
 * A person or organization.
 */
public class Person implements DataModel<PersonType> {
    protected String name;
    protected Email email;
    protected Link link;

    @Override
    public PersonType getData() {
        return null;
    }
}
