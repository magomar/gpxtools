package net.deludobellico.gpxt.app.model.gpx;

import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.PtType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Mario G�mez
 * A geographic point with optional elevation and time.
 */
public class Point implements DataModel<PtType> {
    protected BigDecimal ele;
    protected LocalDateTime time;
    protected BigDecimal lat;
    protected BigDecimal lon;

    @Override
    public PtType getData() {
        return null;
    }
}
