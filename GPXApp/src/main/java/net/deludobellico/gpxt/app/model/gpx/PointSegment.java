package net.deludobellico.gpxt.app.model.gpx;

import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.PtsegType;

import java.util.List;

/**
 * @author Mario G�mez
 * An ordered sequence of points. (for polygons or polylines, e.g.)
 */
public class PointSegment implements DataModel<PtsegType> {
    protected List<Point> pt;
    @Override
    public PtsegType getData() {
        return null;
    }
}
