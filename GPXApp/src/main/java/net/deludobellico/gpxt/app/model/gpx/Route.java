package net.deludobellico.gpxt.app.model.gpx;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.RteType;

import java.util.stream.Collectors;

/**
 * @author Mario G�mez
 *         An ordered list of waypoints representing a series of turn points leading to a destination.
 */
public class Route implements DataModel<RteType> {
    protected StringProperty name = new SimpleStringProperty();
    protected StringProperty description = new SimpleStringProperty();
    protected StringProperty comment = new SimpleStringProperty();
    protected StringProperty source = new SimpleStringProperty();
    protected IntegerProperty number = new SimpleIntegerProperty();
    protected StringProperty type = new SimpleStringProperty();
    protected ObservableList<Link> links = FXCollections.observableArrayList();
    protected ObservableList<Waypoint> waypoints = FXCollections.observableArrayList();
    protected Extensions extensions;

    public Route(RteType route) {
        name.set(route.getName());
        comment.set(route.getCmt());
        description.set(route.getDesc());
        source.set(route.getSrc());
        number.set(route.getNumber().intValue());
        type.set(route.getType());
        extensions = new Extensions(route.getExtensions());
        links.addAll(route.getLink().stream().map(Link::new).collect(Collectors.toList()));
        waypoints.addAll(route.getRtept().stream().map(Waypoint::new).collect(Collectors.toList()));
    }

    @Override
    public RteType getData() {
        RteType route = new RteType();
        route.setName(name.get());
        route.setDesc(description.get());
        route.setCmt(comment.get());
        route.setSrc(source.get());
        route.setType(type.get());
        route.setExtensions(extensions.getData());
        route.getLink().addAll(links.stream().map(Link::getData).collect(Collectors.toList()));
        route.getRtept().addAll(waypoints.stream().map(Waypoint::getData).collect(Collectors.toList()));
        return route;
    }
}
