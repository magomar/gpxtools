package net.deludobellico.gpxt.app.model.gpx;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.TrkType;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mario G�mez
 *         An ordered list of points describing a path. These points are specified as a list of segments.
 */
public class Track implements DataModel<TrkType> {
    private StringProperty name = new SimpleStringProperty();
    private StringProperty description = new SimpleStringProperty();
    private StringProperty comment = new SimpleStringProperty();
    private StringProperty source = new SimpleStringProperty();
    private IntegerProperty number = new SimpleIntegerProperty();
    private StringProperty type = new SimpleStringProperty();
    private ObservableList<Link> links = FXCollections.observableArrayList();
    private ObservableList<TrackSegment> trackSegments = FXCollections.observableArrayList();
    private Extensions extensions;
    private Waypoint startPoint;
    private Waypoint endPoint;

    public Track(TrkType track) {
        name.set(track.getName());
        comment.set(track.getCmt());
        description.set(track.getDesc());
        source.set(track.getSrc());
        if (null != track.getNumber()) number.set(track.getNumber().intValue());
        type.set(track.getType());
        extensions = new Extensions(track.getExtensions());
        links.addAll(track.getLink().stream().map(Link::new).collect(Collectors.toList()));
        trackSegments.addAll(track.getTrkseg().stream().map(TrackSegment::new).collect(Collectors.toList()));
        startPoint = trackSegments.get(0).getWaypoints().get(0);
        List<Waypoint> lastWaypoints = trackSegments.get(trackSegments.size()-1).getWaypoints();
        endPoint = lastWaypoints.get(lastWaypoints.size()-1);
    }

    @Override
    public TrkType getData() {
        TrkType track = new TrkType();
        track.setName(name.get());
        track.setDesc(description.get());
        track.setCmt(comment.get());
        track.setSrc(source.get());
        track.setType(type.get());
        track.setExtensions(extensions.getData());
        track.getLink().addAll(links.stream().map(Link::getData).collect(Collectors.toList()));
        track.getTrkseg().addAll(trackSegments.stream().map(TrackSegment::getData).collect(Collectors.toList()));
        return track;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public String getComment() {
        return comment.get();
    }

    public StringProperty commentProperty() {
        return comment;
    }

    public String getSource() {
        return source.get();
    }

    public StringProperty sourceProperty() {
        return source;
    }

    public int getNumber() {
        return number.get();
    }

    public IntegerProperty numberProperty() {
        return number;
    }

    public String getType() {
        return type.get();
    }

    public StringProperty typeProperty() {
        return type;
    }

    public ObservableList<Link> getLinks() {
        return links;
    }

    public ObservableList<TrackSegment> getTrackSegments() {
        return trackSegments;
    }

    public Extensions getExtensions() {
        return extensions;
    }

    public Waypoint getStartPoint() {
        return startPoint;
    }

    public Waypoint getEndPoint() {
        return endPoint;
    }
}
