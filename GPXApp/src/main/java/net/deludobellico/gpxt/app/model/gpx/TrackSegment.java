package net.deludobellico.gpxt.app.model.gpx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.TrksegType;

import java.util.stream.Collectors;

/**
 * @author Mario G�mez
 * Holds a list of Track Points which are logically connected in order. To represent a single GPS track where
 * GPS reception was lost, or the GPS receiver was turned off, start a new Track Segment for each continuous
 * span of track data.
 */
public class TrackSegment implements DataModel<TrksegType> {
    protected ObservableList<Waypoint> waypoints = FXCollections.observableArrayList();

    public TrackSegment(TrksegType trackSegment) {
        waypoints.addAll(trackSegment.getTrkpt().stream().map(Waypoint::new).collect(Collectors.toList()));
    }


    @Override
    public TrksegType getData() {
        TrksegType trackSegment = new TrksegType();
        trackSegment.getTrkpt().addAll(waypoints.stream().map(Waypoint::getData).collect(Collectors.toList()));
        return trackSegment;
    }

    public ObservableList<Waypoint> getWaypoints() {
        return waypoints;
    }
}















