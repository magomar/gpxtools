package net.deludobellico.gpxt.app.model.gpx;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import net.deludobellico.gpxt.app.util.DateTimeUtils;
import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.app.model.gpxtpx.TrackPointExtension;
import net.deludobellico.gpxt.data.jaxb.TrackPointExtensionT;
import net.deludobellico.gpxt.data.jaxb.WptType;

import javax.xml.bind.JAXBElement;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Mario G�mez
 *         Represents a waypoint, point of interest, or named feature on a map
 */
public class Waypoint implements DataModel<WptType>, Comparable<Waypoint> {
    //    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
//    private static final SimpleDateFormat msdateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'"); // allow millis too
    private DoubleProperty elevation = new SimpleDoubleProperty();
    private ObjectProperty<LocalDateTime> time = new SimpleObjectProperty<>();
    private DoubleProperty latitude = new SimpleDoubleProperty();
    private DoubleProperty longitude = new SimpleDoubleProperty();
    private ObjectProperty<TrackPointExtension> trackPointExtension = new SimpleObjectProperty<>();

    public Waypoint(WptType waypoint) {
        if (null != waypoint.getEle()) elevation.set(waypoint.getEle().doubleValue());
        if (null != waypoint.getTime()) time.set(DateTimeUtils.convertToLocalDateTime(waypoint.getTime()));
        latitude.set(waypoint.getLat().doubleValue());
        longitude.set(waypoint.getLon().doubleValue());
        if (null != waypoint.getExtensions())
            for (Object o : waypoint.getExtensions().getAny()) {
                JAXBElement element = (JAXBElement) o;
                Object extension = element.getValue();
//            System.out.println(extension.getClass().getSimpleName());
                switch (extension.getClass().getSimpleName()) {
                    case "TrackPointExtensionT":
                        trackPointExtension.set(new TrackPointExtension((TrackPointExtensionT) extension));
                }
            }
    }

    @Override
    public WptType getData() {
        WptType waypoint = new WptType();
        waypoint.setLat(BigDecimal.valueOf(latitude.get()));
        waypoint.setLon(BigDecimal.valueOf(longitude.get()));
        if (null != elevation.getValue()) waypoint.setEle(BigDecimal.valueOf(elevation.get()));
        if (null != time.getValue()) waypoint.setTime(DateTimeUtils.toXMLGregorianCalendar(time.get()));
        return waypoint;
    }

    public double getElevation() {
        return elevation.get();
    }

    public DoubleProperty elevationProperty() {
        return elevation;
    }

    public LocalDateTime getTime() {
        return time.get();
    }

    public ObjectProperty<LocalDateTime> timeProperty() {
        return time;
    }

    public double getLatitude() {
        return latitude.get();
    }

    public DoubleProperty latitudeProperty() {
        return latitude;
    }

    public double getLongitude() {
        return longitude.get();
    }

    public DoubleProperty longitudeProperty() {
        return longitude;
    }

    public TrackPointExtension getTrackPointExtension() {
        return trackPointExtension.get();
    }

    public ObjectProperty<TrackPointExtension> trackPointExtensionProperty() {
        return trackPointExtension;
    }

    @Override
    public int compareTo(Waypoint o) {
        int timeComparison = getTime().compareTo(o.getTime());
        if (timeComparison != 0) return timeComparison;
        int latComparison = latitude.getValue().compareTo(o.getLatitude());
        if (latComparison != 0) return latComparison;
        int longComparison = longitude.getValue().compareTo(o.getLongitude());
        if (longComparison != 0) return longComparison;
        return elevation.getValue().compareTo(o.getElevation());
    }
}
