package net.deludobellico.gpxt.app.model.gpxtpx;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import net.deludobellico.gpxt.app.model.DataModel;
import net.deludobellico.gpxt.data.jaxb.TrackPointExtensionT;

/**
 * Created by Mario on 03/06/2015.
 */
public class TrackPointExtension implements DataModel<TrackPointExtensionT> {
    private IntegerProperty hr = new SimpleIntegerProperty();
    private IntegerProperty cad = new SimpleIntegerProperty();

    public TrackPointExtension(TrackPointExtensionT trackPointExtension) {
        if (null != trackPointExtension.getHr()) hr.set(trackPointExtension.getHr());
        if (null != trackPointExtension.getCad()) cad.set(trackPointExtension.getCad());
    }

    @Override
    public TrackPointExtensionT getData() {
        TrackPointExtensionT tpx = new TrackPointExtensionT();
        if (null != hr.getValue()) tpx.setHr((short) hr.get());
        if (null != cad.getValue()) tpx.setCad((short) cad.get());
        return tpx;
    }

    public int getHr() {
        return hr.get();
    }

    public IntegerProperty hrProperty() {
        return hr;
    }

    public int getCad() {
        return cad.get();
    }

    public IntegerProperty cadProperty() {
        return cad;
    }
}
