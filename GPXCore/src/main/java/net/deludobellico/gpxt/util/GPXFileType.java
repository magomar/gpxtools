package net.deludobellico.gpxt.util;

/**
 * Created by Mario on 28/02/2015.
 */
public enum GPXFileType implements FileType {
    GPX(new String[]{".gpx"}, "GPS eXchange Format");

    private final String[] fileExtension;
    private final String description;
    private final FileTypeFilter fileTypeFilter;

    GPXFileType(final String[] fileExtension, final String description) {
        this.fileExtension = fileExtension;
        this.description = description;
        this.fileTypeFilter = new FileTypeFilter(this);
    }

    @Override
    public String[] getFileExtension() {
        return fileExtension;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public FileTypeFilter getFileTypeFilter() {
        return fileTypeFilter;
    }

//    public GPXFileType fromFileExtension(String extension) {
//        if (extension != null) {
//            for (GPXFileType fileType : GPXFileType.values()) {
//                if (extension.equalsIgnoreCase(fileType.fileExtension)) {
//                    return fileType;
//                }
//            }
//        }
//        return null;
//    }
}

